# Vim快速配置

### 修改人

mirrorvim

## 项目说明

该仓库fork于Vimplus,解决通过github下载插件太慢的问题

## 安装

和Vimplus一样,详情见目录下的master.md

## 特点

1. 内置插件无需等待github

2. 修改配置文件添加和修改插件和快捷键

3. tab党(我选择tab)

## 源仓库

[Vimplus](https://gitee.com/chxuan/vimplus.git)

---


